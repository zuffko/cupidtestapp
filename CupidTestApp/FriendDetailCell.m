//
//  FriendDetailCell.m
//  CupidTestApp
//
//  Created by zufa on 5/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "FriendDetailCell.h"


@implementation FriendDetailCell

+(FriendDetailCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"UserInfoCell" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"UserInfoCell";
}

@end
