//
//  PhotoScrollVC.m
//  CupidTestApp
//
//  Created by zufa on 6/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "PhotoScrollVC.h"

@interface PhotoScrollVC () <UIScrollViewDelegate>

@end

@implementation PhotoScrollVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    _scroll.delegate = self;
    [self.view addSubview:_scroll];
    _scroll.minimumZoomScale = 0.4;
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadWithURL:[NSURL URLWithString:_photoUrl]
                     options:0
                    progress:^(NSUInteger receivedSize, long long expectedSize){}
                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                   {
                       if (image)
                       {
                           _scroll.contentSize =CGSizeMake(image.size.width, image.size.height);
                           UIImageView *qwe= [UIImageView new];
                           qwe.userInteractionEnabled = NO;
                           qwe.frame=CGRectMake(0, 0, image.size.width, image.size.height);
                           [qwe setImage:image];
                           [_scroll addSubview:qwe];
                       }
                   }];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return [self.scroll.subviews objectAtIndex:0];
}

@end
