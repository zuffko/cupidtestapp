//
//  FriendDetailTableVC.h
//  CupidTestApp
//
//  Created by zufa on 5/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "UIImage+_roppedImage.h"

@interface FriendDetailTableVC : UITableViewController <FbClient> 
@property (nonatomic, retain) NSString *currentUserId;
-(void)LoadFrendDetailInfo:(NSMutableDictionary *)userInfoDic;
-(void)LoadFrendPhoto:(NSMutableArray *)userPhotos;
@end
