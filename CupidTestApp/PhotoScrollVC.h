//
//  PhotoScrollVC.h
//  CupidTestApp
//
//  Created by zufa on 6/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface PhotoScrollVC : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;
@property (nonatomic, retain) NSString *photoUrl;
@end
