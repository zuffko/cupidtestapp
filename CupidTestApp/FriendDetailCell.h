//
//  FriendDetailCell.h
//  CupidTestApp
//
//  Created by zufa on 5/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface FriendDetailCell : UITableViewCell
+(FriendDetailCell*)cell;
+(NSString *)cellID;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationCity;
@property (weak, nonatomic) IBOutlet UILabel *bithdayDate;
@property (weak, nonatomic) IBOutlet UILabel *profileLink;
@property (weak, nonatomic) IBOutlet UILabel *educationLabel;
@property (weak, nonatomic) IBOutlet FBProfilePictureView *userAvatar;
@end
