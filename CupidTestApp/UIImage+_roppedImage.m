//
//  UIImage+_roppedImage.m
//  CupidTestApp
//
//  Created by zufa on 6/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "UIImage+_roppedImage.h"

@implementation UIImage (_croppedImage)
- (UIImage *)crop:(CGRect)rect {
    
    rect = CGRectMake(rect.origin.x*self.scale,
                      rect.origin.y*self.scale,
                      rect.size.width*self.scale,
                      rect.size.height*self.scale);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef
                                          scale:self.scale
                                    orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}
@end
