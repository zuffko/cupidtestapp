//
//  FacebookFriendsTableViewController.h
//  CupidTestApp
//
//  Created by zufa on 4/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FbClient.h"

@interface FacebookFriendsTableVC : UITableViewController <FbClient>
@property (nonatomic, retain) NSMutableArray  *friendsArray;
@end
