//
//  PhotosCell.h
//  CupidTestApp
//
//  Created by zufa on 6/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosCell : UITableViewCell
+(PhotosCell*)cell;
+(NSString *)cellID;
@property (weak, nonatomic) IBOutlet UIImageView *imageWiew;

@end
