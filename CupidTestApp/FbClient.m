//
//  FbClient.m
//  CupidTestApp
//
//  Created by zufa on 16/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "FbClient.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FacebookFriendsTableVC.h"
#import "AppDelegate.h"
#import "FriendDetailTableVC.h"

@implementation FbClient{
    AppDelegate *appDelegate;
    FBRequest *request;
}

@synthesize delegate;

static FbClient * sharedFbClient = NULL;

+(FbClient *)sharedFbClient {
    if (!sharedFbClient || sharedFbClient == NULL) {
		sharedFbClient = [FbClient new];
	}
	return sharedFbClient;
}

-(void)openFbSession
{
    if (!appDelegate.session.isOpen)
    {
        appDelegate.session = [[FBSession alloc] init];
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded)
        {
            [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            }];
        }
    }
}

-(void)loadFriends
{
    __block  FbClient *blockSelf = self;
    request = [FBRequest requestForGraphPath:@"me/friends?fields=name,link,quotes"];
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        NSMutableArray *userArray = result[@"data"];
        if (!error)
        {
            //[_friendsArray removeAllObjects];
            NSMutableArray *friendsArray = [NSMutableArray new];
            for (FBGraphObject *user in userArray)
            {
                NSMutableDictionary *friend= [[NSMutableDictionary alloc] initWithObjectsAndKeys:user[@"id"],@"userId",
                                              user[@"name"],@"name",
                                              user[@"link"],@"link", nil];
                [friendsArray addObject:friend];
            }
            SEL selector = @selector(loadFriendsArray:);
            [blockSelf.delegate performSelector:selector withObject:friendsArray];
        }
    }];
}

//- (IBAction)fbLogin:(id)sender
//{
//    if (appDelegate.session.isOpen)
//    {
//        [appDelegate.session closeAndClearTokenInformation];
//        [_friendsArray removeAllObjects];
//        [self updateView];
//    }
//    else
//    {
//        if (appDelegate.session.state != FBSessionStateCreatedOpening)
//        {
//            appDelegate.session = [[FBSession alloc] init];
//        }
//        [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
//         {
//             if (appDelegate.session.state != FBSessionStateCreatedOpening)
//             {
//                 [FBSession setActiveSession:session];
//                 [self opnssn];
//             }
//             [self loadFriens];
//         }];
//    }
//}
//
//
-(void)requestForFriend:(NSString*)userId
{
    __block  FbClient *blockSelf = self;
    FBRequest *userinfoRequest = [FBRequest requestForGraphPath:[NSString stringWithFormat:@"%@?fields=name,birthday,location,education,link", (NSString*)userId]];
    [userinfoRequest startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         FBGraphObject *fbGraphObject = (FBGraphObject *)result;
         NSMutableArray *educationArray = result[@"education"];
         NSMutableDictionary *userInfo= [[NSMutableDictionary alloc] initWithObjectsAndKeys:fbGraphObject[@"id"],@"userId",
                    fbGraphObject[@"name"],@"name",
                    fbGraphObject[@"link"],@"link",

                    fbGraphObject[@"birthday"],@"birthday",                    fbGraphObject[@"location"][@"name"],@"location",
                    educationArray.lastObject[@"school"][@"name"],@"education", nil];
         SEL selector = @selector(LoadFrendDetailInfo:);
         [blockSelf.delegate performSelector:selector withObject:userInfo];
     }];
}
//
-(void)requestForPhotos:(NSString*)userId
{
    __block  FbClient *blockSelf = self;
    NSMutableArray *photos = [NSMutableArray new];
    FBRequest *userPhotoRequest = [FBRequest requestForGraphPath:[NSString stringWithFormat:@"%@?fields=photos.limit(30)", (NSString*)userId]];
    [userPhotoRequest startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         FBGraphObject *fbGraphObject = (FBGraphObject *)result;
         FBGraphObject *photosGraphObject = fbGraphObject[@"photos"];
         NSMutableArray *photosArray = photosGraphObject[@"data"];
         for (FBGraphObject *images in photosArray)
         {
             NSMutableDictionary *photosDic= [[NSMutableDictionary alloc] initWithObjectsAndKeys:[images[@"images"] objectAtIndex:2][@"source"],@"bigImage",
                                              [images[@"images"] lastObject][@"source"],@"smallImage",nil];
             [photos addObject:photosDic];
         }
         SEL selector = @selector(LoadFrendPhoto:);
         [blockSelf.delegate performSelector:selector withObject:photos];
     }];
}

@end
