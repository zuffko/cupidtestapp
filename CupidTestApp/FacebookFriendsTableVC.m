//
//  FacebookFriendsTableViewController.m
//  CupidTestApp
//
//  Created by zufa on 4/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//
#import <SDWebImage/UIImageView+WebCache.h>
#import "FacebookFriendsTableVC.h"
#import "AppDelegate.h"
#import "FriendsListCell.h"
#import "FriendDetailTableVC.h"


@interface FacebookFriendsTableVC  (){
    AppDelegate *appDelegate;
    FBRequest *request;
    FbClient *fbClient;
}
@end

@implementation FacebookFriendsTableVC 
@synthesize friendsArray = _friendsArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    _friendsArray = [NSMutableArray new];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLogged) name:@"loginNotification" object:nil];
    appDelegate = [[UIApplication sharedApplication]delegate];
    fbClient = [FbClient new];
    fbClient.delegate = self;
    [fbClient openFbSession];
    [fbClient loadFriends];
}

-(void)loadFriendsArray:(NSMutableArray *)array
{
    _friendsArray=array;
    [self.tableView reloadData];
}



//-(void)opnssn
//{
//    if (!appDelegate.session.isOpen)
//    {
//        appDelegate.session = [[FBSession alloc] init];
//        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded)
//        {
//            [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
//            }];
//        }
//    }
//}
//
//-(void)loadFriens
//{
//    request = [FBRequest requestForGraphPath:@"me/friends?fields=name,link,quotes"];
//    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//        NSMutableArray *userArray = result[@"data"];
//        if (!error)
//        {
//            [_friendsArray removeAllObjects];
//            for (FBGraphObject *user in userArray)
//            {
//                NSMutableDictionary *friend= [[NSMutableDictionary alloc] initWithObjectsAndKeys:user[@"id"],@"userId",
//                                                                                               user[@"name"],@"name",
//                                                                                               user[@"link"],@"link", nil];
//                [_friendsArray addObject:friend];
//            }
//            [self.tableView reloadData];
//            [self updateView];
//        }
//    }];    
//}

- (void)updateView {
    if (appDelegate.session.isOpen)
    {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"logout" style:UIBarButtonItemStyleBordered target:self action:@selector(fbLogin:)];
    }
    else
    {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"login" style:UIBarButtonItemStyleBordered target:self action:@selector(fbLogin:)];
            [self.tableView reloadData];
    }
}

-(void)userLogged
{
    [fbClient loadFriends];
}

- (IBAction)fbLogin:(id)sender
{
    if (appDelegate.session.isOpen)
    {
        [appDelegate.session closeAndClearTokenInformation];
        [_friendsArray removeAllObjects];
        [self updateView];
    }
    else
    {
        if (appDelegate.session.state != FBSessionStateCreatedOpening)
            {
            appDelegate.session = [[FBSession alloc] init];
            }
        [appDelegate.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
        {
        if (appDelegate.session.state != FBSessionStateCreatedOpening)
            {
            [FBSession setActiveSession:session];
            [fbClient openFbSession];
            }
            
            [fbClient loadFriends];
        }];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([_friendsArray count]>0)
    {
        return [_friendsArray count];
    }
    else
    {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FriendsListCell *friendsListCell;
    friendsListCell = [tableView dequeueReusableCellWithIdentifier:[FriendsListCell cellID]];
    if (!friendsListCell)
    {
        friendsListCell=[FriendsListCell cell];
    }
    friendsListCell.nameLabel.text =[_friendsArray objectAtIndex:indexPath.row][@"name"];
    friendsListCell.locationLabel.text =[_friendsArray objectAtIndex:indexPath.row][@"userId"];
    friendsListCell.profilePictureView.profileID = nil;
    friendsListCell.profilePictureView.profileID = [_friendsArray objectAtIndex:indexPath.row][@"userId"];
    return friendsListCell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"MySegue" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"MySegue"])
        {
        FriendDetailTableVC *detailViewController = [segue destinationViewController];
        detailViewController.currentUserId=[_friendsArray objectAtIndex:[self.tableView indexPathForSelectedRow].row][@"userId"];
    }
}

@end
