//
//  UIImage+_roppedImage.h
//  CupidTestApp
//
//  Created by zufa on 6/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (_croppedImage)
- (UIImage *)crop:(CGRect)rect;
@end
