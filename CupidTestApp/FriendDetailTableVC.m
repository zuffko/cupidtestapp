//
//  FriendDetailTableVC.m
//  CupidTestApp
//
//  Created by zufa on 5/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "FriendDetailTableVC.h"
#import "FriendDetailCell.h"
#import "PhotosCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PhotoScrollVC.h"

@interface FriendDetailTableVC (){
    AppDelegate *appDelegate;
    NSMutableDictionary *userInfo;
    NSMutableArray *photos;
    SDWebImageManager *manager;
    FBRequest *request;
}
@end

@implementation FriendDetailTableVC
@synthesize currentUserId=_currentUserId;
- (void)viewDidLoad
{
    [super viewDidLoad];
    manager = [SDWebImageManager sharedManager];
    [FbClient sharedFbClient].delegate = self;
    [[FbClient sharedFbClient] requestForFriend:_currentUserId];
    [[FbClient sharedFbClient] requestForPhotos:_currentUserId];
}

-(void)LoadFrendDetailInfo:(NSMutableDictionary *)userInfoDic
{
    userInfo = (NSMutableDictionary *)userInfoDic;
    [self.tableView reloadData];
}

-(void)LoadFrendPhoto:(NSMutableArray *)userPhotos
{
    photos = (NSMutableArray *)userPhotos;
    [self.tableView reloadData];
}

//-(void)requestForFriend
//{
//    FBRequest *userinfoRequest = [FBRequest requestForGraphPath:[NSString stringWithFormat:@"%@?fields=name,birthday,location,education,link", _currentUserId]];
//    [userinfoRequest startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
//    {
//        FBGraphObject *fbGraphObject = (FBGraphObject *)result;
//        NSMutableArray *educationArray = result[@"education"];
//        userInfo= [[NSMutableDictionary alloc] initWithObjectsAndKeys:fbGraphObject[@"id"],@"userId",
//                                                                    fbGraphObject[@"name"],@"name",
//                                                                    fbGraphObject[@"link"],@"link",
//                                                                fbGraphObject[@"birthday"],@"birthday",
//                                                       fbGraphObject[@"location"][@"name"],@"location",
//                                             educationArray.lastObject[@"school"][@"name"],@"education", nil];
//        [self.tableView reloadData];
//    }];
//}
//
//-(void)requestForPhotos
//{
//    photos = [NSMutableArray new];
//    FBRequest *userPhotoRequest = [FBRequest requestForGraphPath:[NSString stringWithFormat:@"%@?fields=photos.limit(30)", _currentUserId]];
//    [userPhotoRequest startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
//    {
//        FBGraphObject *fbGraphObject = (FBGraphObject *)result;
//        FBGraphObject *photosGraphObject = fbGraphObject[@"photos"];
//        NSMutableArray *photosArray = photosGraphObject[@"data"];
//        for (FBGraphObject *images in photosArray)
//        {
//            NSMutableDictionary *photosDic= [[NSMutableDictionary alloc] initWithObjectsAndKeys:[images[@"images"] objectAtIndex:2][@"source"],@"bigImage",
//                                                                                                     [images[@"images"] lastObject][@"source"],@"smallImage",nil];
//            [photos addObject:photosDic];
//        }
//        [self.tableView reloadData];
//    }];
//}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [photos count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        FriendDetailCell *friendDetailCell;
        friendDetailCell = [tableView dequeueReusableCellWithIdentifier:[FriendDetailCell cellID]];
        if (!friendDetailCell)
        {
            friendDetailCell=[FriendDetailCell cell];
        }
        friendDetailCell.nameLabel.text=userInfo[@"name"];
        friendDetailCell.bithdayDate.text=userInfo[@"birthday"];
        friendDetailCell.locationCity.text=userInfo[@"location"];
        friendDetailCell.profileLink.text=userInfo[@"link"];
        friendDetailCell.educationLabel.text=userInfo[@"education"];
        friendDetailCell.userAvatar.profileID=userInfo[@"userId"];
        return friendDetailCell;
    }
    else
    {
        PhotosCell *photosCell;
        photosCell = [tableView dequeueReusableCellWithIdentifier:[PhotosCell cellID]];
        if (!photosCell)
        {
            photosCell=[PhotosCell cell];
        }
            __weak PhotosCell *wcell = photosCell;
            [manager downloadWithURL:[NSURL URLWithString:[photos objectAtIndex:indexPath.row-1][@"smallImage"]]
                             options:0
                            progress:^(NSUInteger receivedSize, long long expectedSize){}
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                            {
                               if (image)
                               {
                                   UIImage *croppedImage = [image crop:CGRectMake(0, 0, 86, 86)];
                                   [wcell.imageView setImage:croppedImage];
                                   UIImageView *qwe=[UIImageView new];
                                   [qwe setImage:croppedImage];
                                   [wcell addSubview:qwe];
                               }
                           }];
            return photosCell;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row>0)
    {
        [self performSegueWithIdentifier:@"Fullphoto" sender:nil];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"Fullphoto"])
    {
        PhotoScrollVC *detailViewController = [segue destinationViewController];
        detailViewController.photoUrl = [photos objectAtIndex:[self.tableView indexPathForSelectedRow].row-1][@"bigImage"];
    }
}

@end
