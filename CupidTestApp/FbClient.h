//
//  FbClient.h
//  CupidTestApp
//
//  Created by zufa on 16/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FbClient <NSObject>
@end

@interface FbClient : NSObject{
    id <FbClient> __unsafe_unretained delegate;
}

@property (nonatomic, assign) id <FbClient> delegate;

+(FbClient *)sharedFbClient;
-(void)openFbSession;
-(void)loadFriends;
-(void)requestForFriend:(NSString*)userId;
-(void)requestForPhotos:(NSString*)userId;
@end