//
//  FriendsListCell.m
//  CupidTestApp
//
//  Created by zufa on 5/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "FriendsListCell.h"

@implementation FriendsListCell

+(FriendsListCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"friendsListCell" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"friendsListCell";
}


@end
