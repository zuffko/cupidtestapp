//
//  ViewController.m
//  CupidTestApp
//
//  Created by zufa on 3/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        appDelegate.session = [[FBSession alloc] init];
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
                NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
                              appDelegate.session.accessTokenData.accessToken]);
            }];
        }
    }
}


-(IBAction)login
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    if (appDelegate.session.state != FBSessionStateCreated) {

        appDelegate.session = [[FBSession alloc] init];
    }

    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
                      appDelegate.session.accessTokenData.accessToken]);
    }];


}

- (IBAction)frind:(id)sender {
    //[FBSession openActiveSessionWithAllowLoginUI: YES];
    if (!FBSession.activeSession.isOpen) {
        [FBSession openActiveSessionWithAllowLoginUI: YES];
    }
    [[FBRequest requestForMyFriends] startWithCompletionHandler:^(FBRequestConnection *connection,
                                                                  NSDictionary *results,
                                                                  NSError *error)
     {
         if(!error){
        
             NSLog(@"%@", results);
         }else{
             NSLog(@"%@", error);
         }
         
     }];
}

-(IBAction)info:(id)sender{
    [FBSession openActiveSessionWithAllowLoginUI: YES];
    FBRequest *request = [FBRequest requestForGraphPath:@"me/friends?fields=education,name,link,quotes"];
    NSLog(@"%@", request);
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        NSLog(@"%@", result);
    }];
    
    
//    FBRequest *request = [FBRequest requestForGraphPath:@"/me/friends?fields=id,name,age_range"];
//    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
//    request.session = appDelegate.session;
//    NSLog(@"%@", request.parameters);
//    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//        NSLog(@"%@", result);
//    }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
