//
//  PhotosCell.m
//  CupidTestApp
//
//  Created by zufa on 6/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "PhotosCell.h"

@implementation PhotosCell

+(PhotosCell*)cell{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"photosCell" owner:nil options:nil];
    return [nibObjects objectAtIndex:0];
}

+(NSString*)cellID
{
    return @"photosCell";
}

@end
