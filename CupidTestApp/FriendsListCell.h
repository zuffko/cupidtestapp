//
//  FriendsListCell.h
//  CupidTestApp
//
//  Created by zufa on 5/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>
#import <UIKit/UIKit.h>

@interface FriendsListCell : UITableViewCell
@property (retain, nonatomic) IBOutlet FBProfilePictureView *profilePictureView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
+(FriendsListCell*)cell;
+(NSString *)cellID;
@end
