//
//  AppDelegate.m
//  CupidTestApp
//
//  Created by zufa on 3/8/13.
//  Copyright (c) 2013 Sichkar Evgen. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate{
    UIActivityIndicatorView* indicator;
}
@synthesize session = _session;
@synthesize indicator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [FBSession openActiveSessionWithAllowLoginUI:NO];
    [FBProfilePictureView class];
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginNotification" object:nil];
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication withSession:self.session];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [FBSession.activeSession close];
}

- (void)applicationWillResignActive:(UIApplication *)application
{

}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBAppEvents activateApp];
    
    [FBAppCall handleDidBecomeActiveWithSession:self.session];
}


@end
